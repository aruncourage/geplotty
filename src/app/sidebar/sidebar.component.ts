import { Component, OnInit } from '@angular/core';
import {Menus} from './menu';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  private menuCollections;
  public items:any;
  public isActive: boolean = false;
  constructor() { 
    this.menuCollections = new Menus();
  }

  ngOnInit() {
    this.items = this.menuCollections.getMenu();
  }

}
