export class Menus {
    private menus;
    constructor(){
        this.menus =[
            {
                label: 'Dashboard',
                routerLink: ['/'],
                title:'Dashboard' 
            },
            {
                label: 'Plotly', 
                routerLink: ['/plotly'],
                title:'Plotly'
            },
        ];
        
    }
    getMenu(){
        return this.menus;
    }
}
