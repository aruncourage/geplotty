import { TestBed, getTestBed, inject } from '@angular/core/testing';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { PlotlydemoService } from './plotlydemo.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('PlotlydemoService', () => {
  let injector: TestBed;
  let service: PlotlydemoService;
  let httpMock: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
      providers: [PlotlydemoService]
    })
  );

  it('should be created', () => {
    const service: PlotlydemoService = TestBed.get(PlotlydemoService);
    expect(service).toBeTruthy();
  });

  it('expects service to fetch data with PlotlydemoService',
  inject([HttpTestingController, PlotlydemoService],
    (httpMock: HttpTestingController, service:PlotlydemoService) => {
      // We call the service
      service.getHappinesDetails().subscribe(data => {
        expect(data).toEqual(dummyUsers);
      });
      // We set the expectations for the HttpClient mock
      const req = httpMock.expectOne(service.serviceUrl);

      expect(req.request.method).toEqual('GET');
        const dummyUsers = [
        {
          "Country": "Norway",
          "Happiness.Rank": 1,
          "Happiness.Score": 7.53700017929077,
          "Whisker.high": 7.59444482058287,
          "Whisker.low": 7.47955553799868,
          "Economy..GDP.per.Capita.": 1.61646318435669,
          "Family": 1.53352355957031,
          "Health..Life.Expectancy.": 0.796666502952576,
          "Freedom": 0.635422587394714,
          "Generosity": 0.36201223731041,
          "Trust..Government.Corruption.": 0.315963834524155,
          "Dystopia.Residual": 2.27702665328979
        }
    ];
      // Then we set the fake data to be returned by the mock
      req.flush(dummyUsers);
    })
);
});
