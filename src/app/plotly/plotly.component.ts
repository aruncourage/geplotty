import { Component, OnInit } from '@angular/core';
import { PlotlydemoService } from '../plotlydemo.service'

@Component({
  selector: 'app-plotly',
  templateUrl: './plotly.component.html',
  styleUrls: ['./plotly.component.scss']
})
export class PlotlyComponent implements OnInit {
  
  public countryName: any[] = [];
  public happinessRank: any[] = [];
  public happinessScore: any[] = [];
  public happinessFamily: any[] = [];
  public pieChart: any;
  public barChart: any;
  public scatterChart: any;
  public layout: any;
  constructor(private plotlyDemo: PlotlydemoService) { }

  ngOnInit() {
    this.plotlyDemo.getHappinesDetails().subscribe((data: any) => {
      this.happinesDeatils(data);
    });
  }

  happinesDeatils(data) {
    for(var i = 0;i<5;i++) {
      this.countryName.push(data[i].Country);
       this.happinessRank.push(data[i]["Happiness.Rank"]);
       this.happinessScore.push(data[i]["Happiness.Score"]);
       this.happinessFamily.push(data[i]["Happiness.Family"]);
   }
   setTimeout(function(){
     this.loading = true; 
     }, 3000);
   this.pieChartRank();
   this.lineChart();

  }
  pieChartRank() {
     this.pieChart = [{
        values: this.happinessRank,
        labels:this.countryName,
        type: 'pie'
       }];
    this.layout = {width: 400, height: 500, title: 'A Pipe Plot'}
  }

  lineChart() {
    this.barChart = [{
       y: this.happinessScore,
       x:this.countryName,
       type: 'bar'
      }];
   this.layout = {width: 400, height: 500, title: 'A Bar Plot'}
 }
}
